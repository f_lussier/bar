﻿using Bar.Models;
using Bar.Settings;
using MongoDB.Driver;

namespace Bar.Services.Repositories
{
    public class BarContext
    {
        private readonly IMongoDatabase _database;

        public BarContext(IMongoDatabase database) => _database = database;
        
        public IMongoCollection<Bottle> Bottles =>  _database.GetCollection<Bottle>(nameof(Bottle));

        public IMongoCollection<Recipe> Recipes => _database.GetCollection<Recipe>(nameof(Recipe));

        public IMongoCollection<Order> Orders => _database.GetCollection<Order>(nameof(Order));
    }
}
