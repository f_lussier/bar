﻿using MediatR;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Bar.Models
{
    public class OrderCommand : IRequest
    {
        [Required]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("recipe")]
        public string Recipe { get; private set; }

        [Required]
        [JsonProperty("quantity")]
        public int Quantity { get; private set; }

        public OrderCommand(string recipe, int quantity)
        {
            Recipe = recipe;
            Quantity = quantity;
        }
    }
}
