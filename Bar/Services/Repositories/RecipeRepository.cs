﻿using Bar.Models;
using MongoDB.Driver;

namespace Bar.Services.Repositories
{
    public class RecipeRepository : Repository<Recipe, IMongoCollection<Recipe>>
    {
        public RecipeRepository(BarContext context) : base(context.Recipes)
        {
        }
    }
}
