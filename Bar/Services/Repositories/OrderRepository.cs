﻿using Bar.Models;
using MongoDB.Driver;

namespace Bar.Services.Repositories
{
    public class OrderRepository: Repository<Order, IMongoCollection<Order>>
    {
        public OrderRepository(BarContext context) : base(context.Orders)
        {

        }
    }
}
