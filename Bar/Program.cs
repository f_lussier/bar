using System;
using System.Diagnostics;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.Extensions.Hosting;

namespace Bar
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IHost host;
            try
            {
                host = CreateHostBuilder(args).Build();
            }
            catch (Exception ex)
            {
                Debug.Fail("Crash when trying to build"+ ex);
                throw;
            }

            try
            {
                host.Run();
            }
            catch (Exception ex)
            {
                Debug.Fail("Crash when running app" + ex);
                throw;
            }
        }
        
        
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration((webHostBuilderContext, configurationBuilder) =>
                    {
                        configurationBuilder
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");
                    }).UseStartup<Startup>();
                });
    }
}
