﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Bar.Models
{
    public class Recipe : IBarModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("recipe_quantity")]
        public List<RecipeProportion> RecipeQuantities;
        
        public Recipe()
        {
            RecipeQuantities = new List<RecipeProportion>();
        }
    }

    public class RecipeDto
    {
        [JsonIgnore]
        public string Id { get; set; }
        [JsonIgnore]
        public List<Bottle> Bottles;

        public RecipeDto(string id, IEnumerable<Bottle> bottles)
        {
            Id = id;
            Bottles = bottles.ToList();
        }
    }
}