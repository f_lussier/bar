﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
namespace Bar.Models
{
    public class RecipeProportion
    {
        [JsonProperty("quantity")]
        [Range(0,100, ErrorMessage = "La quantité doit être entre 0 et 100%")]
        public float Proportion { get; set; }

        [JsonProperty("bottle")]
        public Bottle Bottle { get; set; }
    }
}
