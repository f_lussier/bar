﻿using System;
using System.Threading.Tasks;
using Bar.Models;
using Bar.Services.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Bar.Controllers
{
    [Route("/[Controller]")]
    public class RecipeController : ControllerBase
    {
        private readonly IRepository<Recipe> _repository;

        public RecipeController(IRepository<Recipe> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetRecipes()
        {
            return Ok(await _repository.List());
        }

        [HttpPost]
        public async Task<IActionResult> CreateRecipe([FromBody] Recipe recipe)
        {
            try
            {
                return Ok(await _repository.AddItem(recipe));
            }
            catch (ArgumentNullException ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRecipe(string id)
        {
            return Ok(await _repository.RemoveItem(id));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAllRecipe()
        {
            return Ok(await _repository.RemoveAllItems());
        }
    }
}
