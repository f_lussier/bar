﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Bar.Models
{
    public class Bottle : IBarModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("marque")]
        public string Marque { get; set; }

        [JsonProperty("quantity")]
        public float Quantity { get; set; }

        [JsonProperty("pump")]
        public int PumpNumber { get; set; }

        public Bottle(string id, string type, string marque, float quantity, int pumpNumber)
        {
            Id = id;
            Type = type;
            Marque = marque;
            Quantity = quantity;
            PumpNumber = pumpNumber;
        }
    }
}