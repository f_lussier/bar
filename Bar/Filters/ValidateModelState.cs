﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Bar.Filters
{
    public class ValidateModelState: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var controller = context.Controller as ControllerBase;
                context.Result = controller?.BadRequest(context.ModelState);
                return;
            }
            base.OnActionExecuting(context);
        }
    }
}
