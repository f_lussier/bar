﻿using System;
using System.Threading.Tasks;
using Bar.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bar.Controllers
{
    [Route("/[Controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpPost]
        public async Task<IActionResult> CreateRecipe([FromBody] OrderCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return StatusCode(204,"La commande est en traitement");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
