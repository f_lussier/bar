﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bar.Services.Repositories
{
    public interface IRepository<T>
    {
        Task<List<T>> List();

        Task<T> Find(string id);

        Task<T> AddItem(T item);

        Task<bool> RemoveItem(string id);

        Task<bool> UpdateItem(T item);

        Task<bool> RemoveAllItems();
    }
}
