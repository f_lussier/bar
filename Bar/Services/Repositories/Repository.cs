using System.Collections.Generic;
using System.Threading.Tasks;
using Bar.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Bar.Services.Repositories
{
    public abstract class Repository<TModel, TCollection> : IRepository<TModel> 
        where TModel: IBarModel
        where TCollection : IMongoCollection<TModel>
    {
        private readonly TCollection _collection;
        protected Repository(TCollection collection) => _collection = collection;

        public virtual Task<List<TModel>> List()
            => _collection.Find(_ => true).ToListAsync();

        public virtual Task<TModel> Find(string id)
            => _collection.Find(note => note.Id == id)
                .FirstOrDefaultAsync();

        public virtual async Task<TModel> AddItem(TModel item)
        {
            await _collection.InsertOneAsync(item);
            return item;
        }

        public virtual async Task<bool> RemoveItem(string id)
        {
            var actionResult = await _collection.DeleteOneAsync(Builders<TModel>.Filter.Eq(nameof(IBarModel.Id), id));
            return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
        }

        public virtual async Task<bool> UpdateItem(TModel item)
        {
            var actionResult = await _collection.ReplaceOneAsync(x => x.Id.Equals(item.Id),
                item, new UpdateOptions { IsUpsert = true });

            return actionResult.IsAcknowledged && actionResult.ModifiedCount > 0;
        }

        public virtual async Task<bool> RemoveAllItems()
        {
            var actionResult = await _collection.DeleteManyAsync(new BsonDocument());
            return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
        }
    }
}