using System;
using System.Threading.Tasks;
using Bar.Models;
using Bar.Services.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Bar.Controllers
{
    [Route("/[Controller]")]
    public class BottleController : ControllerBase
    {
        private readonly IRepository<Bottle> _bottleRepository;
        public BottleController(IRepository<Bottle> bottleRepository)
        {
            _bottleRepository = bottleRepository ?? throw new ArgumentNullException(nameof(bottleRepository));
        }

        [HttpGet]
        public async Task<IActionResult> GetBottles()
        {
            return Ok(await _bottleRepository.List());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBottles(string id)
        {
            return Ok(await _bottleRepository.Find(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateBottle([FromBody] Bottle bottle)
        {
            return Ok(await _bottleRepository.AddItem(bottle));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBottle(string id)
        {
            if (await _bottleRepository.RemoveItem(id))
            {
                return Ok("Bottle removed.");
            }
            return NotFound("Bottle not found.");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> ModifyBottle(Bottle bottle)
        {
            return Ok(await _bottleRepository.UpdateItem(bottle));
        }
    }
}
