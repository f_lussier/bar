﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Bar.Models
{
    public class Order : IBarModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [JsonProperty("id")]
        public string Id { get; set; }

        public DateTime DateCreation { get; private set; }

        public DateTime DateFinished { get; set; }

        public RecipeDto Recipe { get; private set; }

        public Order(RecipeDto Recipe)
        {
            DateCreation = DateTime.Now;
            Recipe = Recipe;
        }
    }
}