﻿using Bar.Models;
using MongoDB.Driver;

namespace Bar.Services.Repositories
{
    public class BottleRepository : Repository<Bottle,IMongoCollection<Bottle>>
    {
        public BottleRepository(BarContext context) : base(context.Bottles)
        {
          
        }
    }
}
