﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bar.Models;
using Bar.Services.Repositories;
using MediatR;

namespace Bar.Handlers
{
    public class OrderCommandHandler : AsyncRequestHandler<OrderCommand>
    {
        private readonly IRepository<Recipe> _recipeRepository;
        private readonly IRepository<Order> _orderRepository;

        public OrderCommandHandler(IRepository<Recipe> recipeRepository, IRepository<Order> orderRepository)
        {
            _recipeRepository = recipeRepository ?? throw new ArgumentNullException(nameof(recipeRepository));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        }

        protected override async Task Handle(OrderCommand command, CancellationToken cancellationToken)
        {
            var recipe = await _recipeRepository.Find(command.Recipe);
            var bottles = recipe.RecipeQuantities
                    .Select(x => new Bottle(x.Bottle.Id,x.Bottle.Type,x.Bottle.Marque,ToPercent(x.Proportion,command.Quantity),x.Bottle.PumpNumber));
     
            var order = new Order(new RecipeDto(command.Recipe,bottles));
            await _orderRepository.AddItem(order);
        }

        private static float ToPercent(float x, int y) => (x * y) / 100;
    }
}
