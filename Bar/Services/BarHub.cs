﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Bar.Services
{
    public class BarHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}
