using System.Net.Http;
using Bar.Filters;
using Bar.Models;
using Bar.Services.Repositories;
using Bar.Settings;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace Bar
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "AllowAll",
                    builder =>
                    {
                        builder
                            .AllowAnyOrigin();
                    });
            });

            services.AddControllers().AddNewtonsoftJson(o => 
            {
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            
            services.AddMediatR(typeof(Startup));
            
            var applicationSettings = new BarSettings();
            _configuration.Bind(applicationSettings);
            services.AddDependency(applicationSettings);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseCors("AllowAll");
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }

    internal static class ServiceExtension
    {
        public static void AddDependency(this IServiceCollection services, BarSettings settings)
        {
            var database = new MongoClient(settings.ConnectionString).GetDatabase(settings.Database);
            services.AddSingleton(new BarContext(database));
            services.AddTransient<IRepository<Bottle>, BottleRepository>();
            services.AddTransient<IRepository<Recipe>, RecipeRepository>();
            services.AddTransient<IRepository<Order>, OrderRepository>();
            services.AddTransient<HttpClient, HttpClient>();
            services.AddScoped<ValidateModelState>();

            services.AddScoped<ServiceFactory>(p => p.GetService);
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));
        }
    }
}